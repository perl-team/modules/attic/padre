#!/bin/sh
#
# Repack upstream source removing the non-DFSG files as described in
# debian/copyright
#
# To be called via debian/watch (uscan or uscan --force)
# or
#  sh debian/repack.sh --upstream-version VER FILE

set -e
set -u

usage() {
    cat <<EOF >& 2
Usage: $0 --upstream-version VER FILE

            or

       uscan [--force]
EOF
}

[ "${1:-}" = "--upstream-version" ] \
    && [ -n "${2:-}" ] \
    && [ -n "${3:-}" ] \
    && [ -z "${4:-}" ] \
    || usage

TMPDIR=`mktemp -d -p .`

trap "rm -rf $TMPDIR" INT QUIT 0

VER="$2"
DEB_VER="${VER}.ds1"
UP_VER="${VER}"
UPSTREAM_TAR="$3"
UPSTREAM_DIR=Padre-${UP_VER}
ORIG="../padre_${DEB_VER}.orig.tar.gz"
ORIG_DIR="padre-${DEB_VER}.orig"

if [ -e "$ORIG" ]; then
    echo "$ORIG already exists. Aborting."
    exit 1
fi

echo -n "Expanding upstream source tree..."
tar xzf $UPSTREAM_TAR -C $TMPDIR
echo " done."

# clean generated files
echo "Cleaning non-DFSG files..."
rm -v $TMPDIR/$UPSTREAM_DIR/share/padre-splash-ccnc.bmp
rm -v $TMPDIR/$UPSTREAM_DIR/share/languages/perl5/perlapi_current.yml

mv $TMPDIR/$UPSTREAM_DIR $TMPDIR/$ORIG_DIR

echo -n Repackaging into ${ORIG} ...
tar c -C $TMPDIR $ORIG_DIR | gzip -n -9 > "$ORIG"
echo " done."
