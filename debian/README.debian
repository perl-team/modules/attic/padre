Changes to upstream source
--------------------------

* Upstream demands wxWidgets 2.8.8, which is not available on Lenny. The
  package seems to work with 2.8.7 but there may be some ill side effects.

* padre 0.35 has a feature allowing controlling of the running padre instance.
  unfortunately it uses a listening TCP socket bound to 127.0.0.1:4444 without
  any authentication. To avoid security problems, that feature is disabled in
  the Debian package.
  The problem is tracked upstream at http://padre.perlide.org/ticket/313

* padre 0.48 includes a copy of a Perl API reference. The Debian package
  replaces this with one generated during build, using Perl::APIReference
  (libperl-apiregerence-perl package).

* the 'primary' padre splash image is distributed under a license
  forbidding commercial usage (i.e. cc-by-nc-sa). Since this violates
  Debian Free Software Guidelines, the image is removed and a
  substitute (also provided upstream) is used.
